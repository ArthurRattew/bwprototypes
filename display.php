<!DOCTYPE html>
<head>
    <title>Display</title>
</head>

<body>
    <?php
      // form that would be equivalent to the embedded iframe
      $radioVal = $_POST['hiddenRadio'];
      echo '<form name="theForm" id="myform" method="post" style="visibility: hidden;" enctype="multipart/form-data" action="printout.php">';
      echo '  <input type="text" name="textHidden" value="|" required="required" style="visibility: hidden;" /><br>';
      if ($radioVal == 1){
        echo '  1- <input type="radio" name="hiddenRadio" value ="1" required="required"  checked="checked" style="visibility: hidden;" /><br>';
      }
      if ($radioVal == 2){
        echo '  2- <input type="radio" name="hiddenRadio" value ="2" required="required" checked="checked" style="visibility: hidden;" /><br>';
      }
      if ($radioVal == 3){
        echo '  3- <input type="radio" name="hiddenRadio" value ="3" required="required" checked="checked"  style="visibility: hidden;" /><br>';
      }
      echo '  <input type="submit" id="formToServer" name="submitButton" style="visibility: hidden;"></form>';
    ?>

    <!-- submits form on page load, repeats operation every tenth of a second untill page is left -->
      <script>
        window.onload=function(){
          var auto = setTimeout(function(){ autoRefresh(); }, 5);
          function submitform(){
            document.theForm.submit();
          }
          function autoRefresh(){
             clearTimeout(auto);
             auto = setTimeout(function(){ submitform(); autoRefresh(); }, 100);
          }
      }
    </script>
</body>
