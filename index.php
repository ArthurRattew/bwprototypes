<!DOCTYPE html>
<head>
    <title>main page</title>
</head>
<body>

  <?php
    // A  brief description for my approach to the "middleman" idea
    // if submit has not been pressed yet, this php creates a form
    // the form created is equivalent to the front end we would have complete control over
    // the code then takes the data from the generated form which posts to this page
    // it then calls another page (display.php), which contains the form which could be equivalent to the iframe
    //the iframe equivalent has the values from this form put into it and then is automatically
    //submit on page load with javascript, the page then displays the collected data on printout.php to show the efficacy
    if (isset($_POST['submitButton'])) {
      $textVal = $_POST['textGroup'];
      $radioVal = $_POST['radioGroup'];

      $post_data['textHidden'] = $textVal;
      $post_data['hiddenRadio'] = $radioVal;

      foreach ( $post_data as $key => $value) {
      $post_items[] = $key . '=' . $value;
      }
      $post_string = implode ('&', $post_items);
      $curl_connection =
      curl_init('http://www.arthurrattewphotography.com/betterweekdays/demo/display.php');
      curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
      curl_setopt($curl_connection, CURLOPT_USERAGENT,
      $_SERVER['HTTP_USER_AGENT']);
      curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);
      $result = curl_exec($curl_connection);
      $result =  (string)$result;
      $returned =  explode("|", $result);
      $valueAdded = $returned[0].$textVal.$returned[1];
      echo $valueAdded;
      curl_close($curl_connection);
    }
    else{
      echo '<h2>Assesment "middleman" idea</h2>';
      echo '<form method="post" enctype="multipart/form-data" action="'.$_SERVER['PHP_SELF'].'">';
      echo '  <input type="text" name="textGroup" required="required" /><br>';
      echo '  1- <input type="radio" name="radioGroup" value = 1 required="required" /><br>';
      echo '  2- <input type="radio" name="radioGroup" value = 2 required="required" /><br>';
      echo '  3- <input type="radio" name="radioGroup" value = 3 required="required" /><br>';
      echo '  <input type="submit" name="submitButton"></form>';
    }
  ?>
</body>
